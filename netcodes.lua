 
chess.net = {
	codes = {
		getGlobalInfo = 1,
		movePiece = 2,
		updateBoard = 3,
		updateClientInfo = 4,
	}
}