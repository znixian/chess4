local net = {}

class = love.filesystem.load("middleclass.lua")()
love.filesystem.load("middleclass-commons.lua")()

love.filesystem.load("LUBE.lua")()

love.filesystem.load("netcodes.lua")()

net.players = {
	list = {},
	byClient = {}
}

function net.draw()
end

-- add a player to the server.
-- * playerInfo: {name=<username>}
-- * client: <the client id returned by the networking library>
local function addPlayer(playerInfo, client)
	playerInfo.id = #net.players.list + 1
	net.players.list[playerInfo.id] = playerInfo
	
	if client ~= nil then
		net.players.byClient[client] = playerInfo
	end
end

function net.addLocalPlayer()
	net.localPlayer = {name="host"}
	addPlayer(net.localPlayer)
end

-- send the board. If the client id is not supplied, the message is sent to all clients.
local function sendBoard(client)
	local board = chess.utils.serBoard()
	net.server:send(binser.serialize(chess.net.codes.updateBoard, board), client)
end

-- send everyone's client info to everyone else.
local function sendClientInfo()
	for clientid, _ in pairs(net.server.clients) do
		if net.players.byClient[clientid] ~= nil then
			local id = net.players.byClient[clientid].id
			net.server:send(binser.s(chess.net.codes.updateClientInfo, net.players.list, id), clientid)
			print("sent to " .. net.players.byClient[clientid].name)
		end
	end
end

local function connCallback(client) --when a client connects
    print("connect from client.")
	addPlayer({name="client" .. #net.players.list}, client)
end

local function rcvCallback(inData, client) --same as in client, but also receives the client ID
	local data = binser.d(inData)
	if data[1] == chess.net.codes.getGlobalInfo then
		sendBoard(client)
		sendClientInfo()
	elseif data[1] == chess.net.codes.movePiece then
		local d = data[2]
		
		if chess.gamedata.board[d.ox][d.oy] == nil 
				or chess.gamedata.board[d.ox][d.oy].piece == nil
				or net.localPlayer == nil then
			return
		end
		if chess.gamedata.board[d.ox][d.oy].piece.owner ~= net.players.byClient[client] then
			return
		end
		net.movePieceNoOwnerCheck(d.ox, d.oy, d.nx, d.ny)
	end
end

local function disconnCallback(client) --when a client disconnects
	print("bye!")
end

local function loadServer()
	local server = common.instance(lube.tcpServer) -- make the server
	net.server = server -- store the server for later
	
	-- set the callbacks, so we know when things happen
	server.callbacks.recv=rcvCallback
	server.callbacks.connect=connCallback
	server.callbacks.disconnect=disconnCallback
	
	-- set our handshake - this must be the same on the
	--    server and client.
	server.handshake = "chess.net.handshake"
	
	-- set a "ping" (where the server and client send
	--   each other a short string so they know that
	--   they are still connected
	server:setPing(true, 6, "chess.net.ping")
	
	-- listen on port 9358
	server:listen(9358)
	
	-- tell the user the port
	print("port: " .. server.port)
end

function net.update(dt)
	net.server:update(dt)
end

-- build the board, ready for use
local function makeBoard()
	local board = chess.gamedata.board
	local pieces = chess.pieces
	
	for x = 0,13 do
		board[x] = {}
		for y = 0,13 do
			board[x][y] = nil
		end
	end

	local function makeSide(xx, yy, w, h)
		for x=xx,(w+xx-1) do
			for y=yy,(h+yy-1) do
				board[x][y] = {piece = nil}
			end
		end
	end
	
	local function makeBackRow(xs, ys, dx, dy, owner)
		for i=1, 8 do
			board[xs][ys].piece = {type=pieces[chess.pieces.order[i]], owner=owner}
			xs = xs + dx
			ys = ys + dy
		end
	end
	
	local function makePawnRow(xs, ys, dx, dy, owner)
		for i=1, 8 do
			board[xs][ys].piece = {type=pieces.pawn, owner=owner}
			xs = xs + dx
			ys = ys + dy
		end
	end
	
	makeSide(3, 0, 8, 3)
	makeBackRow(3, 0, 1, 0, 1)
	makePawnRow(3, 1, 1, 0, 1)
	
	makeSide(0, 3, 3, 8)
	makeBackRow(0, 3, 0, 1, 2)
	makePawnRow(1, 3, 0, 1, 2)
	
	makeSide(3, 11, 8, 3)
	makeSide(11, 3, 3, 8)
	
	makeSide(3, 3, 8, 8)
end

function net.initGame()
	-- set up the ui data
	chess.gamedata = {
		board = {},
		camera = {x = 0, y = 0, size = 42}
	}
	
	-- make the board
	makeBoard()
	
	-- actually start a server
	loadServer()
end

function net.canSelect(x, y)
	if not chess.utils.isLocationValidCell(x, y)
			or chess.gamedata.board[x][y].piece == nil
			or net.localPlayer == nil then
		return false
	end
	return chess.gamedata.board[x][y].piece.owner == net.localPlayer.id
end

function net.movePiece(ox, oy, nx, ny)
	if net.canSelect(ox, oy) then
		net.movePieceNoOwnerCheck(ox, oy, nx, ny)
	end
end

function net.movePieceNoOwnerCheck(ox, oy, nx, ny)
	-- validate the move
	if chess.utils.isMoveValid(ox, oy, nx, ny) then
		-- find the piece
		local piece = chess.gamedata.board[ox][oy].piece
		
		chess.gamedata.board[nx][ny].piece = piece
		chess.gamedata.board[ox][oy].piece = nil
		
		sendBoard() -- update everyone's board
		
		return true -- found it - we're done
	else
		return false
	end
	
end

return net
