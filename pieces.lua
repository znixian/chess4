-- can move in direction
local function moveInDir(poses, owner, x, y, dx, dy, maxMovement)
	local c = 0

	-- move to start with, so we don't check the cell the main piece is on
	x = x + dx
	y = y + dy

	-- while we are on a valid cell
	while chess.utils.isLocationValidCell(x, y) and c ~= maxMovement do
		-- what should be happening?
		local state = chess.utils.shouldStop(x, y, owner)

		-- obstructed, stop now.
		if state == 0 then return end

		-- so we can go here, so record this
		poses[#poses + 1] = {x=x, y=y}

		-- if that would be a capture, we cannot keep going
		if state == 1 then return end

		-- increment the position of the current cell
		x = x + dx
		y = y + dy

		-- increase the counter
		c = c + 1
	end
end

local function moveTo(poses, owner, x, y)
	-- is this a valid position?
	if not chess.utils.isLocationValidCell(x, y) then return end

	-- can we move there?
	local state = chess.utils.shouldStop(x, y, owner)

	-- obstructed, stop now.
	if state == 0 then return end

	-- so we can go here.
	poses[#poses + 1] = {x=x, y=y}
end

local function moveToRel(poses, owner, x, y, ox, oy)
	moveTo(poses, owner, x + ox, y + oy)
end


-- somewhere to store all the pieces
chess.pieces = {
	pawn = {
		getValidPositions = function(x, y, piece)
			local poses = {}

			local dir = chess.utils.fromDirID(piece.pawnDir)

			if chess.utils.isEmpty(x+dir.x, y+dir.y) then
				poses[#poses + 1] = {x=x+dir.x, y=y+dir.y}
			end

			local dir2 = {x=dir.x, y=dir.y}
			local dir3 = {x=dir.x, y=dir.y}

			if dir.x == 0 then
				dir2.x = dir2.x - 1
				dir3.x = dir3.x + 1
			else
				dir2.y = dir2.y - 1
				dir3.y = dir3.y + 1
			end

			if chess.utils.shouldStop(x+dir2.x, y+dir2.y) == 1 then
				poses[#poses + 1] = {x=x+dir2.x, y=y+dir2.y}
			end
			if chess.utils.shouldStop(x+dir3.x, y+dir3.y) == 1 then
				poses[#poses + 1] = {x=x+dir3.x, y=y+dir3.y}
			end

			return poses
		end
	},
	rook = {
		getValidPositions = function(x, y, piece)
			local poses = {}

			moveInDir(poses, piece.owner, x, y,  1,  0)
			moveInDir(poses, piece.owner, x, y, -1,  0)
			moveInDir(poses, piece.owner, x, y,  0,  1)
			moveInDir(poses, piece.owner, x, y,  0, -1)

			return poses
		end
	},
	knight = {
		getValidPositions = function(x, y, piece)
			local poses = {}

			-- straight
			moveToRel(poses, piece.owner, x, y,  1,  2)
			moveToRel(poses, piece.owner, x, y, -1,  2)
			moveToRel(poses, piece.owner, x, y,  1, -2)
			moveToRel(poses, piece.owner, x, y, -1, -2)

			-- diagonal
			moveToRel(poses, piece.owner, x, y,  2,  1)
			moveToRel(poses, piece.owner, x, y,  2, -1)
			moveToRel(poses, piece.owner, x, y, -2,  1)
			moveToRel(poses, piece.owner, x, y, -2, -1)

			return poses
		end
	},
	bishop = {
		getValidPositions = function(x, y, piece)
			local poses = {}

			moveInDir(poses, piece.owner, x, y,  1,  1)
			moveInDir(poses, piece.owner, x, y, -1,  1)
			moveInDir(poses, piece.owner, x, y,  1, -1)
			moveInDir(poses, piece.owner, x, y, -1, -1)

			return poses
		end
	},
	king = {
		getValidPositions = function(x, y, piece)
			local poses = {}

			-- straight
			moveToRel(poses, piece.owner, x, y,  1,  0)
			moveToRel(poses, piece.owner, x, y, -1,  0)
			moveToRel(poses, piece.owner, x, y,  0,  1)
			moveToRel(poses, piece.owner, x, y,  0, -1)

			-- diagonal
			moveToRel(poses, piece.owner, x, y,  1,  1)
			moveToRel(poses, piece.owner, x, y, -1,  1)
			moveToRel(poses, piece.owner, x, y,  1, -1)
			moveToRel(poses, piece.owner, x, y, -1, -1)

			return poses
		end
	},
	queen = {
		getValidPositions = function(x, y, piece)
			local poses = {}

			-- straight
			moveInDir(poses, piece.owner, x, y,  1,  0)
			moveInDir(poses, piece.owner, x, y, -1,  0)
			moveInDir(poses, piece.owner, x, y,  0,  1)
			moveInDir(poses, piece.owner, x, y,  0, -1)

			-- diagonal
			moveInDir(poses, piece.owner, x, y,  1,  1)
			moveInDir(poses, piece.owner, x, y, -1,  1)
			moveInDir(poses, piece.owner, x, y,  1, -1)
			moveInDir(poses, piece.owner, x, y, -1, -1)

			return poses
		end
	},
}

-- load all of the piece images
function chess.loadPieces()

	-- load in all the piece images
	for name, data in pairs(chess.pieces) do
		data.name = name

		-- make a array for the piece images
		data.images = {}

		-- for each of the teams
		for i=1, 4 do
			local function titleCase( first, rest )
			   return first:upper()..rest:lower()
			end

			local imageName = name:gsub( "(%a)([%w_']*)", titleCase )


			-- load the image
			-- TODO only load once, then clone it for each team
			local img = love.graphics.newImage("images/pieces/" .. imageName .. "/" .. imageName .. "Colours.png")

			-- apply the color transform to it
			img:getData():mapPixel(function( x, y, r, g, b, a )
				if r == 255 and g == 0 and b == 0 then
							if i == 1 or i == 3 then
								r = 255
								g = 255
								b = 255
							else
								r = 0
								g = 0
								b = 0
							end
				elseif r == 0 and g == 255 and b == 0 then
					r = 100
					g = 100
					b = 100
					if i == 1 then r = 255 end
					if i == 2 then g = 255 end
					if i == 3 then b = 255 end
					if i == 4 then r = 255; g = 255 end
				end

				return r,g,b,a
			end)

			-- refresh the image so the changes take effect
			img:refresh()

			-- store it
			data.images[i] = img
		end

		function data:getImage(piece)
			return self.images[piece.owner]
		end
	end
	chess.pieces.order = {"rook", "knight", "bishop", "king", "queen", "bishop", "knight", "rook"}
end
