local net = {}

net.players = {}

net.currentPlayer = nil

-- add a player to the server.
-- * playerInfo: {name=<username>}
function net.addPlayer(playerInfo)
	playerInfo.id = #net.players + 1
	net.players[playerInfo.id] = playerInfo

	if net.currentPlayer == nil then
		net.currentPlayer = playerInfo.id
	end
end

function net.update(dt)
end

function net.draw()
	love.graphics.setColor(0, 0, 0, 255)
	local player = net.players[net.currentPlayer]
	love.graphics.print(player.name, 10, 10)
end

function net.getPlayerTurn()
	return net.currentPlayer
end

-- build the board, ready for use
local function makeBoard()
	local board = chess.gamedata.board
	local pieces = chess.pieces

	for x = 0,13 do
		board[x] = {}
		for y = 0,13 do
			board[x][y] = nil
		end
	end

	local function makeSide(xx, yy, w, h)
		for x=xx,(w+xx-1) do
			for y=yy,(h+yy-1) do
				board[x][y] = {piece = nil}
			end
		end
	end

	local function makeBackRow(xs, ys, dx, dy, owner)
		for i=1, 8 do
			board[xs][ys].piece = {type=pieces[chess.pieces.order[i]], owner=owner}
			xs = xs + dx
			ys = ys + dy
		end
	end

	local function makePawnRow(xs, ys, dx, dy, owner, pawnDir)
		for i=1, 8 do
			local piece = {type=pieces.pawn, owner=owner, pawnDir=pawnDir}
			board[xs][ys].piece = piece
			xs = xs + dx
			ys = ys + dy
		end
	end

	makeSide(3, 0, 8, 3)
	makeBackRow(3, 0, 1, 0, 1)
	makePawnRow(3, 1, 1, 0, 1, 2)

	makeSide(0, 3, 3, 8)
	makeBackRow(0, 3, 0, 1, 2)
	makePawnRow(1, 3, 0, 1, 2, 1)

	makeSide(3, 11, 8, 3)
	makeBackRow(3,13, 1, 0, 3)
	makePawnRow(3,12, 1, 0, 3, 4)

	makeSide(11, 3, 3, 8)
	makeBackRow(13, 3, 0, 1, 4)
	makePawnRow(12, 3, 0, 1, 4, 3)

	makeSide(3, 3, 8, 8)
end

function net.initGame()
	-- set up the ui data
	chess.gamedata = {
		board = {},
		camera = {x = 0, y = 0, size = 42}
	}

	-- make the board
	makeBoard()
end

function net.canSelect(x, y)
	if not chess.utils.isLocationValidCell(x, y)
			or chess.gamedata.board[x][y].piece == nil then
		return false
	end
	return chess.gamedata.board[x][y].piece.owner == net.currentPlayer
end

function net.movePiece(ox, oy, nx, ny)
	if net.canSelect(ox, oy) then
		if net.movePieceNoOwnerCheck(ox, oy, nx, ny) then
			-- make the next player active
			net.currentPlayer = net.currentPlayer + 1
			if net.currentPlayer > #net.players then
				net.currentPlayer = 1
			end
		end
	end
end

function net.movePieceNoOwnerCheck(ox, oy, nx, ny)
	-- validate the move
	if chess.utils.isMoveValid(ox, oy, nx, ny) then
		-- find the piece
		local piece = chess.gamedata.board[ox][oy].piece

		-- move the piece
		chess.gamedata.board[nx][ny].piece = piece
		chess.gamedata.board[ox][oy].piece = nil

		return true -- found it - we're done
	else
		return false
	end

end

return net
