binser = require "binser"
serialize = require "ser"


-- a variable to store everything about the game in
chess = {
	-- somewhere to store our assets (eg, images, sounds)
	assets = {
		-- somewhere to store images
		images = {
			board1 = {}
		}
	},
	
	-- somewhere to store the in-game data (like piece positions), so we can keep it seperate from everything else
	gamedata = nil
}

-- load in some utility functions from "functions.lua"
assert( love.filesystem.load("functions.lua") )( )
assert( love.filesystem.load("pieces.lua") )( )

assert( love.filesystem.load("client-ingame.lua") )( )

-- load assets into memory as the game starts up
function love.load()
	-- load the board image
	chess.assets.images.board1.light = love.graphics.newImage("images/board1-light.png")
	chess.assets.images.board1.dark = love.graphics.newImage("images/board1-dark.png")
	
	-- load all the pieces
	chess.loadPieces()
	
	-- set the background color
	love.graphics.setBackgroundColor(100,100,100)

	-- set the window title
	love.window.setTitle("4-Player Chess")
	
	-- set the window to fullscreen mode
-- 	love.window.setFullscreen(true, "desktop")
end

love.draw = function()
	love.graphics.print("Click to start a game...", love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
end

love.mousepressed = function(x, y, button)
	local net
	
	net = love.filesystem.load("hotseat.lua")()
	for i=1, 4 do
		net.addPlayer({name="player " .. i})
	end
-- 	if button == 1 then
-- 		net = love.filesystem.load("server.lua")()
-- 		net.addLocalPlayer()
-- 	else
-- 		net = love.filesystem.load("client.lua")()
-- 	end
	
	-- start the game
	chess.client.beginGame(net)
end
