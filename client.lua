local net = {
	playerid = nil,
	players = {}
}

class = love.filesystem.load("middleclass.lua")()
love.filesystem.load("middleclass-commons.lua")()

love.filesystem.load("LUBE.lua")()

love.filesystem.load("netcodes.lua")()

function net.draw()
end

local function rcvCallback(inData, ip, port) --same as in client, but also receives ip and port of sender
	local data = binser.d(inData)
	if data[1] == chess.net.codes.updateBoard then
		local serBoard = data[2]
		local board = chess.utils.deserBoard(serBoard)
		chess.gamedata.board = board
	elseif data[1] == chess.net.codes.updateClientInfo then
		net.players = data[2]
		net.playerid = data[3]
		
		print(net.playerid)
	end
end

local function loadClient()
	local client = common.instance(lube.tcpClient) -- make the client
	net.client = client -- store the client for later
	
	client.handshake = "chess.net.handshake" --this is a unique string that will be sent when connecting and disconnecting
	
	-- set a "ping" (where the server and client send
	--   each other a short string so they know that
	--   they are still connected
	client:setPing(true, 2, "chess.net.ping")
	
	-- set rcvCallback as the callback for received messages
	client.callbacks.recv = rcvCallback
	
	-- actually connect
	local res, err = client:connect("localhost", -- hostname
						9358, -- port
						false) -- should the hostname be resolved via DNS
	
	if not res then
		print(err)
	end
end

-- build the board, ready for use
local function loadBoard()
	net.client:send(binser.serialize(chess.net.codes.getGlobalInfo))
end

function net.update(dt)
	net.client:update(dt)
end

function net.initGame()
	-- set up the ui data
	chess.gamedata = {
		camera = {x = 0, y = 0, size = 42}
	}
	
	-- actually start a server
	loadClient()
	
	-- load the board from the server
	loadBoard()
end

function net.canSelect(x, y)
	if not chess.utils.isLocationValidCell(x, y)
			or chess.gamedata.board[x][y].piece == nil
			or net.localPlayer == nil then
		return false
	end
	return chess.gamedata.board[x][y].piece.owner == net.playerid
end

function net.movePiece(ox, oy, nx, ny)
	if chess.utils.isMoveValid(ox, oy, nx, ny) then
		local data = {ox=ox, oy=oy, nx=nx, ny=ny}
		net.client:send(binser.serialize(chess.net.codes.movePiece, data))
		return true
	else
		return false
	end
end

return net
 
