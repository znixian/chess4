chess.client = {
	callbacks = {}
}

-- load assets into memory as the game starts up
function chess.client.beginGame(network)
	-- store the network system for later
	chess.client.network = network

	-- set the background color
	love.graphics.setBackgroundColor(255,255,255)

	-- set up the game data
	network.initGame()

	-- change the callbacks, so that we are now in game
	love.update = chess.client.callbacks.update
	love.draw = chess.client.callbacks.draw
	love.mousepressed = chess.client.callbacks.mousepressed
end

function chess.client.callbacks.update(dt)
	chess.client.network.update(dt)
end

-- draw the screen
function chess.client.callbacks.draw()
	-- clear the screen
	love.graphics.clear(255, 255, 255)

	-- draw any networking-specific stuff
	chess.client.network.draw()

	local mousex, mousey = chess.utils.screenToBoard(love.mouse.getX(), love.mouse.getY())

	if chess.gamedata == nil then
		return
	end
	if chess.gamedata.board == nil then
		love.graphics.setColor(0, 0, 0, 255)
		love.graphics.print("Waiting for server...")
		return
	end

	for x=0,13 do
		for y=0,13 do
			local cell = chess.gamedata.board[x][y]
			if cell ~= nil then
				love.graphics.setColor(255, 255, 255, 255)
				local sx, sy = chess.utils.boardToScreen(x, y)
				local boardScale = chess.gamedata.camera.size / 66
				local pieceScale = chess.gamedata.camera.size / 32
				if (x + y) % 2 == 1 then
					love.graphics.draw(chess.assets.images.board1.light, sx, sy, 0, boardScale, boardScale)
				else
					love.graphics.draw(chess.assets.images.board1.dark, sx, sy, 0, boardScale, boardScale)
				end
				if cell.piece ~= nil then
					love.graphics.draw(cell.piece.type:getImage(cell.piece), sx, sy, 0, pieceScale, pieceScale)

					if mousex == x and mousey == y and chess.client.network.canSelect(x, y) then
						love.graphics.setColor(0, 0, 0, 120)
						love.graphics.rectangle("fill", sx, sy, chess.gamedata.camera.size, chess.gamedata.camera.size)
					end
				end
			end
		end
	end

	if chess.gamedata.selected ~= nil then
		local x = chess.gamedata.selected.x
		local y = chess.gamedata.selected.y

		if not chess.utils.isLocationValidCell(x, y) or chess.gamedata.board[x][y].piece == nil then
			-- the piece no longer exists
			chess.gamedata.selected = nil
		else
			local sx, sy = chess.utils.boardToScreen(x, y)
			love.graphics.setColor(255, 0, 0, 120)
			love.graphics.rectangle("fill", sx, sy, chess.gamedata.camera.size, chess.gamedata.camera.size)
			local piece = chess.gamedata.board[x][y].piece
			local positons = piece.type.getValidPositions(x, y, piece)
			for _, pos in ipairs(positons) do
				local px, py = chess.utils.boardToScreen(pos.x, pos.y)

				local piece2 = chess.gamedata.board[pos.x][pos.y].piece
				if piece2 ~= nil then
					love.graphics.setColor(255, 0, 0, 120)
				else
					love.graphics.setColor(0, 255, 0, 120)
				end

				love.graphics.rectangle("fill", px, py, chess.gamedata.camera.size, chess.gamedata.camera.size)
			end
		end
	end
end

-- handle when the mouse is pressed over a piece
function chess.client.callbacks.mousepressed(x, y, button)

	-- find the board position of the mouse
	local bx, by = chess.utils.screenToBoard(love.mouse.getX(), love.mouse.getY())

	-- is the user left clicking?
	if button == 1 then
		-- reset the selected position
		chess.gamedata.selected = nil

		-- is the user allowed to select that piece?
		if chess.client.network.canSelect(bx, by) then
			-- is it inside the board area?
			if chess.utils.isLocationValidCell(bx, by) then
				-- get the piece
				local piece = chess.gamedata.board[bx][by].piece

				-- is there something there?
				if piece ~= nil then
					-- select it
					chess.gamedata.selected = {x=bx, y=by}
				end
			end
		end
	elseif button == 2 then -- right clicking?
		if chess.gamedata.selected ~= nil then
			-- old X and Y
			local ox = chess.gamedata.selected.x
			local oy = chess.gamedata.selected.y

			-- tell the networking system to move the piece
			local moved = chess.client.network.movePiece(ox, oy, bx, by)

			-- if the piece was moved, deselect it
			if moved then
				-- unselect the piece we just moved
				chess.gamedata.selected = nil
			end
		end
	end
end
