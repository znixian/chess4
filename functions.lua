-- set up out namespace
chess.utils = {}

-- declare some useful utility functions

function chess.utils.screenToBoard(x, y)
	-- to find the board location of a position, we need
	--   to take the position, add the camera offset, and then divide
	--   by the cell size
	
	return math.floor((x - chess.gamedata.camera.x) / chess.gamedata.camera.size),
		math.floor((y - chess.gamedata.camera.y) / chess.gamedata.camera.size)
end

function chess.utils.boardToScreen(x, y)
	-- to find the board location of a position, we need
	--   to take the position, add the camera offset, and then divide
	--   by the cell size
	
	return x * chess.gamedata.camera.size + chess.gamedata.camera.x,
		y * chess.gamedata.camera.size + chess.gamedata.camera.y
end

-- checks to see if the specified position is empty, and a piece can be moved there
function chess.utils.isEmpty(x, y)
	if chess.gamedata.board[x][y] == nil then return end
	if chess.gamedata.board[x][y].piece ~= nil then return end
	return true
end

-- checks to see if the specified position is empty, and a piece can be moved there
-- returns: 0 if we can't move, 1 means we can capture, 2 means its clear
function chess.utils.shouldStop(x, y, owner)
	-- if there is no cell
	if chess.gamedata.board[x][y] == nil then return 0 end
	
	-- if there is a piece in that location
	if chess.gamedata.board[x][y].piece ~= nil then
		-- can't capture our own pieces
		if chess.gamedata.board[x][y].piece.owner == owner then
			return 0
		end
		
		-- we can move there, but not beyond
		return 1
	end
	
	-- otherwise, we can move there
	return 2
end

-- turn a direction ID into a x,y direction (the ID moves clockwise from right)
function chess.utils.fromDirID(id)
	if id == 1 then
		return {x=1,y=0}
	elseif id == 2 then
		return {x=0,y=1}
	elseif id == 3 then
		return {x=-1,y=0}
	elseif id == 4 then
		return {x=0,y=-1}
	end
end

-- turn the board into a sendable state
function chess.utils.serBoard()
	local ser = {}
	for x=0,13 do
		ser[x] = {}
		for y=0,13 do
			local cell = chess.gamedata.board[x][y]
			if cell == nil then
				ser[x][y] = nil
			elseif cell.piece ~= nil then
				ser[x][y] = {type=cell.piece.type.name, owner=cell.piece.owner}
			else
				ser[x][y] = 0
			end
		end
	end
	
	return ser
end

-- turn the board from a sendable state to a usable state
function chess.utils.deserBoard(ser)
	local board = {}
	for x=0,13 do
		board[x] = {}
		for y=0,13 do
			local cell = ser[x][y]
			if cell == 0 then
				board[x][y] = {}
			elseif cell ~= nil then
				board[x][y] = {piece={type=chess.pieces[cell.type], owner=cell.owner}}
			end
		end
	end
	
	return board
end

function chess.utils.isMoveValid(ox, oy, nx, ny)
	-- find the piece
	local piece = chess.gamedata.board[ox][oy].piece
	
	-- get the list of valid positions
	local positons = piece.type.getValidPositions(ox, oy, piece)
	for _, pos in ipairs(positons) do
		if pos.x == nx and pos.y == ny then
			return true -- yep - the move is valid
		end
	end
	
	-- no - the move is invalid
	return false
end

function chess.utils.isLocationValidCell(x, y)
	return x >= 0 and y >= 0 and x <= 13 and y <= 13 and chess.gamedata.board[x][y] ~= nil
end
